import React, { Component } from 'react';
import './App.css';
import AppBar from './components/AppBar';
import Search from './components/Search';
import Sort from './components/Sort';
import HotelCard from './components/HotelCard';
import resource from './models/resource';

const hotelStore = resource('hotels');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hotels: []
    };
  }

  handleSearch = (city, checkin, checkout) => {
    Promise.all([
      hotelStore.create({
        city: city,
        checkin: checkin,
        checkout: checkout,
        provider: 'snaptravel'
      })
        .then( (res) => res.data),

      hotelStore.create({
        city: city,
        checkin: checkin,
        checkout: checkout,
        provider: 'retail'
      })
        .then( (res) => res.data),
    ])
      .then( (res) => {
        let matchingHotels = res[0].hotels.filter( (snaptravelHotel) => {
          for (let retailHotel of res[1].hotels) {
            if (snaptravelHotel.id === retailHotel.id) {
              return true;
            }
          }
          return false;
        })
        return{ matching: matchingHotels, retail: res[1].hotels };
      })
      .then( (hotels) => {
        let matchingHotels = hotels.matching.map( (hotel) => {
          hotel.retail_price = hotels.retail.find( (retailHotel) => {
            return hotel.id === retailHotel.id;
          }).price;
          return hotel;
        });
        return matchingHotels;
      })
      .then( (hotels) => {
        this.setState({
          ...this.state,
          hotels: hotels
        });
      })
      .catch( (err) => console.error(err));
  };

  handleSort = (sortBy) => {
    const hotels = this.state.hotels.sort( (a, b) => {
      switch(sortBy) {
        case 'price':
          return a.price - b.price;
        case 'rating':
          return b.num_reviews - a.num_reviews;
        case 'savings':
          return (b.retail_price - b.price) - (a.retail_price - a.price);
        default:
          return 0;
      }
    });
    this.setState({
      ...this.state,
      hotels: hotels
    });
  };

  render() {
    const state = this.state;
    return (
      <div>
        <AppBar />
        <Search handleSearch={this.handleSearch} />
        {state.hotels[0] && <Sort handleSort={this.handleSort} />}
        {state.hotels.map( (hotel) => {
          return <HotelCard key={hotel.id} hotel={hotel} />
        })}
      </div>
    );
  }
}

export default App;
