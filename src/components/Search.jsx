import React, { Component } from 'react';
import './Search.css';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: '',
      checkinDate: '',
      checkoutDate: ''
    };
  }

  handleChange = (e) => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const {city, checkinDate, checkoutDate} = this.state;
    if (city && checkinDate && checkoutDate) {
      this.props.handleSearch(city, checkinDate, checkoutDate);
    }
  };

  render() {
    return(
      <form onSubmit={this.handleSubmit}>
        <input type='text' id='city' name='city' placeholder='Search By City' value={this.state.city} onChange={this.handleChange} />
        <br />
        <input type='text' id='check-in' name='checkinDate' placeholder='Check-In' value={this.state.checkinDate} onChange={this.handleChange} />
        <input type='text' id='check-out' name='checkoutDate' placeholder='Check-Out' value={this.state.checkoutDate} onChange={this.handleChange} />
        <button type='submit' value='Submit'>
          <i className="fa fa-search" aria-hidden="true"></i>
        </button>
      </form>
    );
  }
};

export default Search;
