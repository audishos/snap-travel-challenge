import React from 'react';
import './HotelCard.css';

const HotelCard = (props) => {
  const hotel = props.hotel;

  let retailClass = hotel.retail_price > hotel.price ? 'more-expensive' : '';
  let starsArray = [];
  for (let i = 0; i < hotel.num_stars; i++) {
    starsArray.push(<i className="fa fa-star" aria-hidden="true" key={i}></i>);
  }

  return(
    <div className='hotel-card'>
      <div className='hotel-content-container'>
        <div className='hotel-details-container'>
          <img className='hotel-image' src={hotel.image_url} alt='hotel' />
          <h1 className='hotel-title'>{hotel.hotel_name}</h1>
          <div className='hotel-amenities'>
            {props.hotel.amenities.map( (amenity, index) => {
              return <p className='hotel-amenity' key={index}>{amenity}</p>;
            })}
          </div>
          <p className='hotel-address'>{hotel.address}</p>
          <br />
          <div className='hotel-rating-container'>
            {starsArray.map( (star) => {
              return star;
            })}
          </div>
          <br />
          <p className='hotel-reviews'>{props.hotel.num_reviews} Reviews</p>
        </div>
        <div className='hotel-prices-container'>
          <div className='retail-price'>
            <h1 className={retailClass}>USD ${hotel.retail_price}</h1>
            <div className='hotel-com-link'><span style={{color: '#F04C3E'}}>Hotels</span><span style={{color: '#000'}}>.com</span></div>
          </div>
          <div className='our-price'>
            <h1>USD ${hotel.price}</h1>
            <div className='hotel-details-button'>View Details</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HotelCard;
