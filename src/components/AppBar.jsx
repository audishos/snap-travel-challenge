import React from 'react';
import './AppBar.css';

const AppBar = (props) => {
  return(
    <div className='app-bar'>
      <div className='app-bar-logo'>
        <img src='/assets/logo_small.png' alt='logo' />
      </div>
      <div className='app-bar-title'>
        <h1>SnapTravel</h1>
      </div>
    </div>
  );
};

export default AppBar;
