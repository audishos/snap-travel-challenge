import React, { Component } from 'react';
import './Sort.css';

class Sort extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortBy: ''
    };
  }

  handleSortClick = (e) => {
    e.preventDefault();
    if (e.target.id !== this.state.sortBy) {
      this.props.handleSort(e.target.id);
      this.setState({
        ...this.state,
        sortBy: e.target.id
      });
    }
  }

  render() {
    const state = this.state;
    return(
      <div>
        <button id='price' className={state.sortBy === 'price' ? 'active' : undefined} onClick={this.handleSortClick}>
          Price
          {state.sortBy === 'price' && <i className="fa fa-sort-amount-asc" aria-hidden="true"></i>}
        </button>
        <button id='rating' className={state.sortBy === 'rating' ? 'active' : undefined} onClick={this.handleSortClick}>
          Rating
          {state.sortBy === 'rating' && <i className="fa fa-sort-amount-desc" aria-hidden="true"></i>}
        </button>
        <button id='savings' className={state.sortBy === 'savings' ? 'active' : undefined} onClick={this.handleSortClick}>
          Savings
          {state.sortBy === 'savings' && <i className="fa fa-sort-amount-desc" aria-hidden="true"></i>}
        </button>
      </div>
    );
  }
}

export default Sort;
